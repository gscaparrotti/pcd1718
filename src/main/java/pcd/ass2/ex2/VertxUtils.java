package pcd.ass2.ex2;

import io.vertx.core.AsyncResult;

public class VertxUtils {
    public static <T> void HandleError(AsyncResult<T> res){
        if(res.failed()) res.cause().printStackTrace();
    }
}
