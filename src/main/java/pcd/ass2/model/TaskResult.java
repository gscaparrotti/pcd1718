package pcd.ass2.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class TaskResult {
    private HashMap<String,Integer> map;

    public TaskResult(){ this.map = new HashMap<>(); }

    public void put(String k, Integer v){
        map.put(k,v);
    }

    public void merge(TaskResult other){
        for(String k : other.map.keySet()){
            this.map.merge(k, other.map.get(k), (x,y) -> x+y);
        }
    }

    public int totAnalysedFiles(){ return map.size(); }
    public int totMatchingFiles(){ return (int)map.values().stream().filter(n -> n>0).count(); }
    public double percMatchingFiles(){ return (0.0+totMatchingFiles())/totAnalysedFiles(); }
    public double meanMatches(){
        return map.values().stream().filter(n -> n>0).collect(Collectors.averagingInt(x -> x));
    }

    public int totMatches() {
        return map.values().stream().collect(Collectors.summingInt(x -> x));
    }

    public List<String> matchedFiles() {
        List<String> result = new ArrayList<>();
        result.addAll(map.keySet());
        return result;
    }
}
