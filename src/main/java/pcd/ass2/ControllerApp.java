package pcd.ass2;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.Subject;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.MessageConsumer;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import pcd.ass2.ex0.FilesystemRegexpSearcher;
import pcd.ass2.ex1.ForkJoinFolderSearcher;
import pcd.ass2.ex1.ForkJoinSearcherSource;
import pcd.ass2.ex2.*;
import pcd.ass2.model.DataPiece;
import pcd.ass2.model.TaskData;
import pcd.ass2.model.TaskDataFull;
import pcd.ass2.model.TaskResult;
import pcd.ass2.utils.K;
import pcd.ass2.utils.Pair;
import pcd.ass2.utils.RxUtils;
import pcd.ass2.utils.Utils;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class ControllerApp implements Initializable {
    @FXML private TextField basePath;
    @FXML private ChoiceBox<Integer> searchDepth;
    @FXML private TextField regexp;
    @FXML private Button start;
    @FXML private Button stop;
    @FXML private ListView matchingFiles;
    @FXML private Label meanMatches;
    @FXML private Label percMatchingFiles;
    @FXML private Label timeToComplete;
    @FXML private ChoiceBox<String> solution;
    @FXML private String ex0;
    @FXML private String ex1a; @FXML private String ex1b;
    @FXML private String ex2a; @FXML private String ex2b;
    @FXML private String ex3a; @FXML private String ex3b;

    Vertx vertx;
    MessageConsumer<String> mc;

    Disposable activity;
    Thread activityThread;
    AtomicBoolean stopCondition = new AtomicBoolean(false);
    long activityStartTime;

    final int updateGuiEveryItems = 1;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /*
        Vertx.clusteredVertx(new VertxOptions().setClustered(true).setClusterHost("127.0.0.1").setHAEnabled(true).setHAGroup("dev"), v -> {
            vertx = v.result();
        });
        */
        vertx = Vertx.vertx();

        start.setOnAction(event -> {
            prepareGuiForTask();
            TaskData data = getDataFromGUI();

            String request = solution.getValue();

            System.out.println("\n--------------"+request+"\nSTART:\n* " + data.getBasePath() + "\n* " + data.getDepth()+ "\n* " + data.getRegex().pattern());

            activityStartTime = System.currentTimeMillis();

            if(request == ex1a) {
                solutionForkJoin(data);
            } else if(request == ex1b){
                solutionForkJoinEvents(data);
            } else if(request == ex2a){
                solutionWithVertx(data);
            } else if(request == ex2b){
                solutionWithVertxOneVerticle(data);
            } else if(request == ex3a){
                solutionReactiveOnBackground(data);
            } else if(request == ex3b){
                solutionReactive(data);
            } else {
                solutionSimpleOnBackground(data);
            }
        });

        stop.setOnAction(event -> {
            if(activity!=null) activity.dispose();
            stopCondition.set(true);
            stop.setDisable(true);
            start.setDisable(false);
            vertx.deploymentIDs().forEach(vertx::undeploy);
            if(mc!=null && mc.isRegistered()) mc.unregister();
            if(fjtask!=null && !fjtask.isCancelled()) fjtask.cancel(true);
        });
    }

    public void solutionWithVertx(TaskData data){
        final FlowableProcessor<DataPiece> subject = PublishProcessor.<DataPiece>create().toSerialized();
        activity = subscribeToProcessingEvents(subject, updateGuiEveryItems);

        // NOTE: ensure there is just one consumer to prevent stealing of events
        mc = vertx.eventBus().consumer(Channels.CHANNEL_EVENT, msg -> {
            String[] parts = msg.body().toString().split(":");
            subject.onNext(new DataPiece(parts[0], Integer.parseInt(parts[1])));
        });

        // TODO: handle ENDgit
        K k = new K(0);
        vertx.eventBus().consumer(Channels.CHANNEL_END, msg -> {
            Integer toDo = Integer.parseInt(msg.body().toString());
            k.add(toDo);
            if(k.get()==0) { // TODO: may incorrectly enter here
                System.out.println("Vertx solution finished.");
                //vertx.deploymentIDs().forEach(vertx::undeploy);
                //subject.onComplete();
            }
        });

        Verticle searcherVerticle = new VerticleFileSearcher(data);
        Verticle analyzerVerticle = new VerticleFileAnalyzer(data);
        vertx.deployVerticle(searcherVerticle);
        vertx.deployVerticle(analyzerVerticle);
    }

    public void solutionWithVertxOneVerticle(TaskData data){
        final FlowableProcessor<DataPiece> subject = PublishProcessor.<DataPiece>create().toSerialized();
        activity = subscribeToProcessingEvents(subject, updateGuiEveryItems);

        /*
        Verticle v = new JustOneVerticle(data, subject);
        vertx.deployVerticle(v);
        */
        File[] files = Utils.getFiles(data.getBasePath(), data.getDepth()).toArray(new File[]{});
        TaskDataFull fullData = new TaskDataFull(data.getBasePath(), data.getDepth(), data.getRegex(), files);
        Verticle v = new JustFileProcessingVerticle(fullData, subject);
        vertx.deployVerticle(v);

        // Handle completion
        RxUtils.completionOnLimit(subject, files.length);
    }

    ForkJoinTask fjtask;
    public void solutionForkJoin(TaskData data){
        ForkJoinPool fjpool = new ForkJoinPool();
        TaskResult taskOutput = new TaskResult();
        fjtask = fjpool.submit(new ForkJoinFolderSearcher(data, result -> {
            setGuiOnCompleteTask();
            updateGui(result);
        }, dataPiece -> {
            // TODO: do something for each (file,count) pair
        }));
    }

    public void solutionForkJoinEvents(TaskData data){
        final FlowableProcessor<DataPiece> subject = PublishProcessor.<DataPiece>create().toSerialized();
        activity = subscribeToProcessingEvents(subject, updateGuiEveryItems);
        ForkJoinPool fjpool = new ForkJoinPool();
        int howMany = fjpool.invoke(new ForkJoinSearcherSource(data, subject));

        RxUtils.completionOnLimit(subject, howMany);
    }

    public void solutionReactive(TaskData data){
        Flowable<File> fileStream = Flowable.create(emitter -> {
            List<File> files = Utils.getFiles(data.getBasePath(), data.getDepth());
            for(File f: files){
                emitter.onNext(f);
            }
            emitter.onComplete();
        }, BackpressureStrategy.BUFFER);

        int coreCount = Runtime.getRuntime().availableProcessors();
        AtomicInteger assigner = new AtomicInteger(0);

        activity = subscribeToProcessingEvents(
            fileStream
                .subscribeOn(Schedulers.io())
                .groupBy(i -> assigner.incrementAndGet() % coreCount)
                .flatMap(grp ->
                    grp.observeOn(Schedulers.io())
                       .map(file -> new Pair<String,String>(file.getPath().replace(data.getBasePath(),""),
                                                            new String(Files.readAllBytes(file.toPath()))))
                            .observeOn(Schedulers.computation())
                       .map(pair -> new DataPiece(pair.first, Utils.numMatches(data.getRegex(), pair.second))))
            , updateGuiEveryItems);
    }

    public void solutionReactiveOnBackground(TaskData data){
        String startingPath = data.getBasePath();
        int depth = data.getDepth();
        Pattern pattern = data.getRegex();

        final FlowableProcessor<DataPiece> subject = PublishProcessor.<DataPiece>create().toSerialized();
        activity = subscribeToProcessingEvents(subject, updateGuiEveryItems);

        Executors.newSingleThreadExecutor().submit(() -> {
            List<File> files = Utils.getFiles(startingPath, depth);
            stopCondition = new AtomicBoolean(false);
            for(File file : files){
                try {
                    if(stopCondition.compareAndSet(true,false)) break;
                    String content = new String(Files.readAllBytes(file.toPath()));
                    int numMatches = Utils.numMatches(pattern, content);
                    subject.onNext(new DataPiece(file.toString().replace(startingPath,""), numMatches));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            subject.onComplete();
        });
    }

    public Disposable subscribeToProcessingEvents(Flowable<DataPiece> flow, int freq){
        final K analysedFiles = new K(0);
        final K numMatchedFiles = new K(0);
        final K numMatchings = new K(0);
        final List<String> matchedFiles = new ArrayList<>();

        /*
        // One may want to update the GUI at some configurabl frequence
        io.reactivex.Observable<Long> updateSignal = io.reactivex.Observable.interval(freq, TimeUnit.MILLISECONDS);
        Disposable updateTicks = updateSignal.forEach(first -> {
            updateGui(analysedFiles.get(), numMatchedFiles.get(), numMatchings.get(), matchedFiles, true);
        });
        */

        // ASSUMPTION: each DataPiece comes only once per file
        Flowable<Integer> nums = Flowable.range(0, Integer.MAX_VALUE);
        return flow
            .onBackpressureBuffer(1000, () -> System.out.println("OVERFLOW"))
            .observeOn(Schedulers.computation())
            .zipWith(nums, (d, counter) -> new Pair<DataPiece,Integer>(d,counter))
            .subscribe(d -> {
                        List<String> files = new ArrayList<>();
                        analysedFiles.add(1);
                        if(d.first.getCount()>0) {
                            numMatchedFiles.add(1);
                            numMatchings.add(d.first.getCount());
                            matchedFiles.add(d.first.getPath());
                            files.add(d.first.getPath());
                        }
                        //if(d.getValue()%freq==0) // update GUI every 'freq' events (not time)
                        updateGui(analysedFiles.get(), numMatchedFiles.get(), numMatchings.get(), files, false);
                    },
                    (error) -> { System.err.println(error); setGuiOnCompleteTask(); },
                    () -> { System.out.println("DONE."); updateGui(analysedFiles.get(), numMatchedFiles.get(), numMatchings.get(), matchedFiles, true); setGuiOnCompleteTask(); });
    }

    /**
     * Simple task on background doing the job.
     */
    public void solutionSimpleOnBackground(TaskData data){
        activityThread = new Thread(new FilesystemRegexpSearcher(data, stopCondition,
                taskResult -> updateGui(taskResult),
                taskResult -> {
                    setGuiOnCompleteTask();
                }));
        activityThread.start();
    }

    public void updateGui(TaskResult tr){
        updateGui(tr.totAnalysedFiles(), tr.totMatchingFiles(), tr.totMatches(), tr.matchedFiles(), true);
    }

    public void updateGui(int all, int nmatching, int nmatches, List<String> files, boolean clear){
        Platform.runLater(() -> {
            if(clear) matchingFiles.getItems().clear();
            matchingFiles.getItems().addAll(files);
            percMatchingFiles.setText(String.format("%.2f", 100.0*nmatching/all) +
                    "% : " + nmatching + " out of " + all);
            meanMatches.setText(String.format("%.2f", (double)nmatches/nmatching));
            final long doneTime = System.currentTimeMillis();
            timeToComplete.setText(""+(doneTime-activityStartTime)+"ms");
        });
    }

    public void resetGui() {
        matchingFiles.getItems().clear();
        percMatchingFiles.setText("");
        meanMatches.setText("");
        timeToComplete.setText("");
    }

    public void prepareGuiForTask(){
        resetGui();
        start.setDisable(true);
        stop.setDisable(false);
    }

    public void setGuiOnCompleteTask(){
        Platform.runLater(() -> {
            start.setDisable(false);
            stop.setDisable(true);
        });
    }

    public TaskData getDataFromGUI(){
        String startingPath = basePath.getText();
        int depth = searchDepth.getValue();
        String re = regexp.getText();
        Pattern pattern = Pattern.compile(re);
        return new TaskData(startingPath, depth, pattern);
    }
}
