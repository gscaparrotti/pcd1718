package pcd.lab08.rx;

import javax.swing.*;
import java.awt.event.*;

public class TestWithSwingTBC {	
	static class MyFrame extends JFrame {	

		public MyFrame(){
			super("Test Swing thread");
			setSize(150,60);
			setVisible(true);
			JButton button = new JButton("Press me");
			button.addActionListener((ActionEvent ev) -> {
				System.out.println("Pressed!");
			});
			getContentPane().add(button);
			addWindowListener(new WindowAdapter(){
				public void windowClosing(WindowEvent ev){
					System.exit(-1);
				}
			});
		}
	}

	static public void main(String[] args){
		whoAmI("main");
		SwingUtilities.invokeLater(()->{
			new MyFrame();
		});
	}

	private static void whoAmI(String where) {
		System.out.println("Who Am I? : "+Thread.currentThread().getName()+" in "+where );
	}
}
