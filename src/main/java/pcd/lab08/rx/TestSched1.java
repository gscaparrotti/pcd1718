package pcd.lab08.rx;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.*;
import io.reactivex.schedulers.*;

public class TestSched1 {

	public static void main(String[] args) throws Exception {
		
		/* running on th main thread */
		
		System.out.println("RUNNING on main thread: ");
		doTest(Observable.just("a","b","c","d","e"));
		
		/* running on an executor based scheduler */
		
		int nThreads = Runtime.getRuntime().availableProcessors();
		ExecutorService exec = Executors.newFixedThreadPool(nThreads);
		Scheduler sched = Schedulers.from(exec);

		System.out.println("RUNNING on sched: ");

		doTest(Observable.just("a","b","c","d","e")
				.subscribeOn(sched)
				.doFinally(exec::shutdown));

		System.out.println("done.");

	}
	
	private static void doTest(Observable<String> obs) {
		obs.map((v) -> {
			System.out.println(Thread.currentThread().getName()+" "+v);
			return v+v;
		})
		.map((v) -> {
			System.out.println(Thread.currentThread().getName()+" "+v);
			return v.length();
		})
		.subscribe((v) -> { 
			System.out.println(Thread.currentThread().getName()+" "+v);
		});
		
	}

}
