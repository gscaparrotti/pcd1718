package pcd.lab03.sync2;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class BoundedBuffer {

	private LinkedList<Integer> buffer;
	private Semaphore availItems;
	private Semaphore availPlaces;
	private Semaphore mutex;
	
	public BoundedBuffer(int size){
		buffer = new LinkedList<Integer>();
		availItems = new Semaphore(0);
		availPlaces = new Semaphore(size);
		mutex= new Semaphore(1);
	}
	
	public void put(Integer el) throws InterruptedException {
		availPlaces.acquire();
		mutex.acquire();
		buffer.add(el);
		mutex.release();
		availItems.release();
	}
	
	public Integer take() throws InterruptedException  {
		availItems.acquire();
		mutex.acquire();
		Integer el = buffer.removeFirst();
		mutex.release();
		availPlaces.release();
		return el;		
	}
	
}
