package pcd.lab06.vertx;

import io.vertx.core.Vertx;

public class Test2 {

	public static void main(String[] args) {
		Vertx  vertx = Vertx.vertx();
		Server myVerticle = new Server();
		vertx.deployVerticle(myVerticle);
	}

}
