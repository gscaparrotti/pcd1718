package pcd.lab04.monitors.cyclic_barrier;

public interface Barrier {

	void hitAndWaitAll() throws InterruptedException;

}
