package pcd.lab04.monitors.cyclic_barrier.sol;

public class BarrierImpl implements Barrier {

	private int nHits;
	private int nParticipants;
	private boolean broken;
	
	public BarrierImpl(int nParticipants) {
		this.nParticipants = nParticipants;
		broken = false;
		nHits = 0;
	}

	@Override
	public synchronized void hitAndWaitAll() throws InterruptedException {
		while (broken) {
			wait();
		}
		nHits++;
		if (nHits == nParticipants) {
			broken = true;
			nHits--;
			notifyAll();
		} else {
			while (nHits < nParticipants && !broken) {
				wait();
			}
			nHits--;
			if (nHits == 0) {
				broken = false;
				notifyAll();
			}
		}
	}	
}
